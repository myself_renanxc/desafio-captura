from bs4 import BeautifulSoup
import requests
import csv
def url_dominio():
	return "http://www.epocacosmeticos.com.br/"

def extrai_titulo(soup):
	tag = soup.find("title", text=True)
	if not tag:
		return None
	return tag.string.strip()

def extrai_nome(soup):
	tag = soup.find("div", class_="productName")
	if not tag:
		return None
	return tag.string.strip()

def extrai_links(soup):
	links = set()
	for tag in soup.find_all("a", href=True):
		if tag["href"].startswith(url_dominio()):
			links.add(tag["href"])
	return links

def inicia_csv():
	planilha = csv.writer(open("produtos_epoca.csv", "w"))
	planilha.writerow(["Título","Nome","URL"])
	return planilha

def crawl(start_url):
	planilha = inicia_csv()
	urls_vistas = set([start_url])
	urls_disponiveis = set([start_url])

	while urls_disponiveis:
		url = urls_disponiveis.pop()
		try:
			conteudo = requests.get(url).text
			soup = BeautifulSoup(conteudo, "lxml")
		except Exception:
			continue

		if url.endswith('/p'):
			linha = [extrai_titulo(soup),extrai_nome(soup),url]
			planilha.writerow(linha)

		for link in extrai_links(soup):
			if link not in urls_vistas:
				urls_vistas.add(link)
				urls_disponiveis.add(link)

if __name__ == '__main__':
	try:
		crawl(url_dominio())
	except KeyboardInterrupt:
		print()
		print("Bye!")
		print()