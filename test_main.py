from main import extrai_titulo, extrai_nome, extrai_links, inicia_csv
from bs4 import BeautifulSoup
import csv
def index_exemplo():
	return '"<!DOCTYPE html><html<head><title>teste</title></head><body><a href=""></a><a href="index.html"></a><a href="pag2.html"></a><div class="productName">testando</div></body></html>"'
def test_extrai_titulo():
	soup = BeautifulSoup(index_exemplo(), "lxml")
	assert "teste" == extrai_titulo(soup)

def test_extrai_nome():
	soup = BeautifulSoup(index_exemplo(), "lxml")
	assert "testando" == extrai_nome(soup)

def test_extrai_links():
	soup = BeautifulSoup(index_exemplo(), "lxml")
	assert None != extrai_links(soup)

def test_inicia_csv():
	assert inicia_csv != None
